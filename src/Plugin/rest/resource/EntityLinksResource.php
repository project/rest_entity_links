<?php

namespace Drupal\rest_entity_links\Plugin\rest\resource;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\AccessAwareRouterInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns entity links that are available to the current user.
 *
 * @RestResource(
 *   id = "entity_links",
 *   label = @Translation("Entity Links"),
 *   uri_paths = {
 *     "canonical" = "/rest/entity_links"
 *   }
 * )
 */
class EntityLinksResource extends ResourceBase {

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Path\PathValidatorInterface $pathValidator
   *   The path validator service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $serializer_formats, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, PathValidatorInterface $pathValidator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->entityTypeManager = $entity_type_manager;
    $this->pathValidator = $pathValidator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager'),
      $container->get('path.validator'),
    );
  }

  /**
   * Responds to entity GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request) {
    $entity = NULL;
    try {
      if ($request->query->has('entity_type')) {
        if ($request->query->has('entity_id')) {
          $storage = $this->entityTypeManager->getStorage($request->query->get('entity_type'));
          $entity = $storage->load($request->query->get('entity_id'));
        }
      }
      elseif ($request->query->has('path')) {
        $url_object = $this->pathValidator ->getUrlIfValid($request->query->get('path'));
        if ($url_object) {
          $route_name = $url_object->getRouteName();
          $route_name_parts = explode('.', $route_name);
          if (count($route_name_parts) >= 3 && $route_name_parts[0] == 'entity') {
            $entity_type = $route_name_parts[1];
            $route_parameters = $url_object->getRouteParameters();
            if (isset($route_parameters[$entity_type])) {
              $entity = $this->entityTypeManager->getStorage($entity_type)
                ->load($route_parameters[$entity_type]);
            }
          }
        }
      }
    } catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->logger->warning($e->getMessage());
    }

    $response = NULL;
    if ($entity) {
      $data = [];
      $cache_dependencies = new CacheableMetadata();
      $cache_dependencies->addCacheableDependency($entity);
      foreach ($entity->uriRelationships() as $relation_name) {
        try {
          $url = $entity->toUrl($relation_name);
          if ($url->access()) {
            $generator_url = $url
              ->setAbsolute(TRUE)
              ->toString(TRUE);
            $cache_dependencies->addCacheableDependency($generator_url);
            $data[$relation_name] = $generator_url->getGeneratedUrl();
          }
        } catch (EntityMalformedException $e) {
          $this->logger->warning($e->getMessage());
        }
      }
      $response = new ResourceResponse($data, 200);
      $response->addCacheableDependency($cache_dependencies);
    }

    if (!$response) {
      $response = new ResourceResponse([], 404);
    }

    $response->addCacheableDependency($request->query->get(AccessAwareRouterInterface::ACCESS_RESULT));
    return $response;
  }

}
